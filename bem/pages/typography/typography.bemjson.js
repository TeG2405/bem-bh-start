module.exports = {
    block: 'page',
    title: 'Текстовая страница',
    id: 'INDEX',
    styles: [{elem: 'css', url: '../_merged/_merged.css'}],
    scripts: [{elem: 'js', url: '../_merged/_merged.async.js', async: true},
        {elem: 'js', url: '../_merged/_merged.js'}, {elem: 'js', url: '../_merged/_merged.i18n.ru.js'}],
    content: [

        {block: 'container', content: [
            {block: 'h', size: '1', content: 'Только типографика'},
            {block: 'hr'},
            {block: 'h', size: '1'},
            {block: 'img', attrs: {align: 'left'}, src: [
                'http://placehold.it/400x200'
            ]},
            {block: 'p'},
            {block: 'p'},
            {block: 'img', attrs: {align: 'right'}, src: [
                'http://placehold.it/200x200'
            ]},
            {block: 'p'},
            {block: 'p'},
            {block: 'p'},
            {block: 'img', src: [
                'http://placehold.it/1500x200'
            ]},
            {block: 'p'},
            {block: 'h', size: '2'},
            {block: 'p'},
            {block: 'h', size: '3'},
            {block: 'p'},
            {block: 'h', size: '4'},
            {block: 'p'},
            {block: 'h', size: '5'},
            {block: 'p'},
            {block: 'h', size: '6'},
            {block: 'p'},
            {block: 'ul', content: [
                'Пункт 1',
                ['Пункт 2', [
                    {block: 'ul', content: [
                        'Пункт 1',
                        'Пункт 2',
                        'Пункт 3'
                    ]}
                ]],
                'Пункт 3'
            ]},
            {block: 'p'},
            {block: 'ol', content: [
                'Пункт 1',
                ['Пункт 2 (вложеный)', [
                    {block: 'ol', content: [
                        'Пункт 1',
                        'Пункт 2',
                        'Пункт 3'
                    ]}
                ]],
                'Пункт 3'
            ]},
            {block: 'p'},
            {block: 'p'},
            {block: 'hr'},
            {block: 'a', content: 'ссылка в тексте (default)'},
            {block: 'br'},
            {block: 'a', mods: {state: 'hover'}, content: 'ссылка в тексте (hover)'},
            {block: 'hr'},
            {block: 'strong', content: 'важный текст'},
            {block: 'br'},
            {block: 'small', content: 'малый текст'},
            {block: 'hr'},
            {block: 'p'},
            {block: 'h', size: '3', content: 'Таблица для bitrix'},
            {block: 'table', content: [
                {elem: 'tbody', content: [
                    [['Ячейка', 'th'], ['Ячейка', 'th'], ['Ячейка', 'th'], ['Ячейка', 'th'], ['Ячейка', 'th']],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]}
            ]},
            {block: 'hr'},
            {block: 'hr'},
            {block: 'hr'},
            {block: 'hr'},
            ///
            {block: 'h', size: '1', content: 'Формы'},
            {block: 'form-group', content: [
                {block: 'control-label',
                    content: 'Лейбл поля'
                },
                {block: 'form-control', content: 'Текстовое поле по умолчанию'}
            ]},
            {block: 'form-group', content: [
                {block: 'control-label',
                    content: 'Лейбл поля'
                },
                {block: 'form-control', content: [
                    'Пункт селекта 1',
                    'Пункт селекта 2',
                    'Пункт селекта 3',
                    'Пункт селекта 4',
                    'Пункт селекта 5'
                ]}
            ]},
            {block: 'form-group', content: [
                {block: 'control-label',
                    content: 'Лейбл поля'
                },
                {block: 'form-control', tag: 'textarea', content: 'Текст'}
            ]},
            {block: 'form-group', content: [
                {block: 'checkbox',
                    name: 'check_1',
                    checked: true,
                    content: 'Чекбокс'
                },
                {block: 'checkbox',
                    name: 'check_2',
                    content: 'Чекбокс'
                },
                {block: 'checkbox',
                    name: 'check_1',
                    disabled: true,
                    content: 'Чекбокс (disabled)'
                }
            ]},
            {block: 'form-group', content: [
                {block: 'radio',
                    name: 'radio_1',
                    content: 'Радио',
                    checked: true
                },
                {block: 'radio',
                    name: 'radio_1',
                    content: 'Радио'
                },
                {block: 'radio',
                    name: 'radio_1',
                    content: 'Радио (disabled)',
                    disabled: true
                }
            ]},

            ///
            {block: 'h', size: '1', content: 'Простые доп. блоки'},
            {block: 'hr'},
            {block: 'form-horizontal', content: [
                {block: 'form-group', content: [
                    {block: 'control-label',
                        mix: {block: 'row', elem: 'col', mods: {xs: 12, sm: 4}},
                        content: 'Лейбл поля'
                    },
                    {
                        mix: {block: 'row', elem: 'col', mods: {xs: 12, sm: 8}}, content: [
                        {block: 'form-control', content: [
                            'Пункт селекта 1',
                            'Пункт селекта 2',
                            'Пункт селекта 3',
                            'Пункт селекта 4',
                            'Пункт селекта 5'
                        ]}
                    ]}
                ]},
                {block: 'form-group', content: [
                    {block: 'control-label',
                        mix: {block: 'row', elem: 'col', mods: {xs: 12, sm: 4}},
                        content: 'Лейбл поля'
                    },
                    {
                        mix: {block: 'row', elem: 'col', mods: {xs: 12, sm: 8}}, content: [
                        {block: 'form-control', content: 'Текстовое поле по умолчанию'},
                        {block: 'checkbox',
                            name: 'check_1',
                            checked: true,
                            content: 'Запомнить выбор'
                        }
                    ]}
                ]},
                {block: 'form-group', content: [
                    {block: 'control-label',
                        mix: {block: 'row', elem: 'col', mods: {xs: 12, sm: 4}}
                    },
                    {
                        mix: {block: 'row', elem: 'col', mods: {xs: 12, sm: 8}}, content: [
                        {block: 'btn', mods: {color: 'primary'}, content: 'Отправить'}
                    ]}
                ]}
            ]},
            {block: 'hr'},
            {block: 'row', content: [
                {elem: 'col', mods: {xs: 12, sm: 6}, content: [
                    {block: 'list-unstyled', content: [
                        {block: 'btn', mods: {color: 'default'}, content: 'Стандартна кнопка'},
                        {block: 'btn', mods: {color: 'primary'}, content: 'Целева кнопка'},
                        {block: 'btn', mods: {color: 'warning'}, content: 'Warning кнопка'},
                        {block: 'btn', mods: {color: 'danger'}, content: 'Danger кнопка'},
                        {block: 'btn', mods: {color: 'info'}, content: 'Info кнопка'},
                        {block: 'btn', mods: {color: 'success'}, content: 'Success кнопка'}
                    ]}
                ]},
                {elem: 'col', mods: {xs: 12, sm: 6}, content: [
                    {block: 'list-unstyled', content: [
                        {block: 'btn', mods: {color: 'default'}, mix: {block: 'active'}, content: 'Стандартна кнопка (hover)'},
                        {block: 'btn', mods: {color: 'primary'}, mix: {block: 'active'}, content: 'Целева кнопка (hover)'},
                        {block: 'btn', mods: {color: 'warning'}, mix: {block: 'active'}, content: 'Warning кнопка (hover)'},
                        {block: 'btn', mods: {color: 'danger'},  mix: {block: 'active'}, content: 'Danger кнопка (hover)'},
                        {block: 'btn', mods: {color: 'info'},    mix: {block: 'active'}, content: 'Info кнопка (hover)'},
                        {block: 'btn', mods: {color: 'success'}, mix: {block: 'active'}, content: 'Success кнопка (hover)'}
                    ]}
                ]}
            ]},
            {block: 'hr'},
            {block: 'list-unstyled', content: [
                {block: 'btn', mods: {size: 'lg'}, content: 'Большого размера'},
                [
                    {block: 'row', content: [
                        {elem: 'col', mods: {xs: 6}, content: [
                            {block: 'btn', mods: {size: 'md'}, content: 'Стандартная (По высоте равна input полю)'}
                        ]},
                        {elem: 'col', mods: {xs: 6}, content: [
                            {block: 'form-control', content: 'Текстовое поле по умолчанию'},
                        ]}
                    ]}

                ],
                {block: 'btn', mods: {size: 'sm'}, content: 'Маленькая'},
                {block: 'btn', mods: {size: 'xs'}, content: 'Самя маленькая'}
            ]},
            {block: 'hr'},
            {block: 'table', content: [
                {elem: 'caption', content: 'Таблица для верстальщика (default)'},
                {elem: 'thead', content: [
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]},
                {elem: 'tbody', content: [
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]},
                {elem: 'tfoot', content: [
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]}
            ]},
        ]}
    ]
};

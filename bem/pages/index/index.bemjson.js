module.exports = {
    block: 'page',
    title: 'Главная',
    id: 'INDEX',
    styles: [{elem: 'css', url: '../_merged/_merged.css'}],
    scripts: [{elem: 'js', url: '../_merged/_merged.async.js', async: true},
        {elem: 'js', url: '../_merged/_merged.js'}, {elem: 'js', url: '../_merged/_merged.i18n.ru.js'}],
    content: [

        {block: 'container', content: [
            {block: 'h', size: '1', content: 'Только типографика'},
            {block: 'hr'},
            {block: 'h', size: '1'},
            {block: 'img', attrs: {align: 'left'}, src: [
                'http://placehold.it/400x200'
            ]},
            {block: 'p'},
            {block: 'p'},
            {block: 'img', attrs: {align: 'right'}, src: [
                'http://placehold.it/200x200'
            ]},
            {block: 'p'},
            {block: 'p'},
            {block: 'p'},
            {block: 'img', src: [
                'http://placehold.it/1500x200'
            ]},
            {block: 'p'},
            {block: 'h', size: '2'},
            {block: 'p'},
            {block: 'h', size: '3'},
            {block: 'p'},
            {block: 'h', size: '4'},
            {block: 'p'},
            {block: 'h', size: '5'},
            {block: 'p'},
            {block: 'h', size: '6'},
            {block: 'p'},
            {block: 'ul', content: [
                'Пункт 1',
                ['Пункт 2', [
                    {block: 'ul', content: [
                        'Пункт 1',
                        'Пункт 2',
                        'Пункт 3'
                    ]}
                ]],
                'Пункт 3'
            ]},
            {block: 'p'},
            {block: 'ol', content: [
                'Пункт 1',
                ['Пункт 2 (вложеный)', [
                    {block: 'ol', content: [
                        'Пункт 1',
                        'Пункт 2',
                        'Пункт 3'
                    ]}
                ]],
                'Пункт 3'
            ]},
            {block: 'p'},
            {block: 'p'},
            {block: 'hr'},
            {block: 'a', content: 'ссылка в тексте (default)'},
            {block: 'br'},
            {block: 'a', mods: {state: 'hover'}, content: 'ссылка в тексте (hover)'},
            {block: 'hr'},
            {block: 'strong', content: 'важный текст'},
            {block: 'br'},
            {block: 'small', content: 'малый текст'},
            {block: 'br'},
            {block: 'br'},
            {block: 'span', content: [
                'Обычная строка текста с',
                {block: 'b', content: ' жирной '},
                'вставкой'
            ]},
            {block: 'hr'},
            {block: 'p'},
            {block: 'h', size: '3', content: 'Таблица для bitrix'},
            {block: 'table', content: [
                {elem: 'tbody', content: [
                    [['Ячейка', 'th'], ['Ячейка', 'th'], ['Ячейка', 'th'], ['Ячейка', 'th'], ['Ячейка', 'th']],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]}
            ]},
            {block: 'hr'},
            {block: 'hr'},
            {block: 'hr'},
            {block: 'hr'},
            ///
            {block: 'h', size: '1', content: 'Формы'},
            {block: 'form-group', content: [
                {block: 'form-control', content: 'Текстовое поле по умолчанию'}
            ]},
            {block: 'form-group', content: [
                {block: 'form-control', content: [
                    'Пункт селекта 1',
                    'Пункт селекта 2',
                    'Пункт селекта 3',
                    'Пункт селекта 4',
                    'Пункт селекта 5',
                ]}
            ]},
            {block: 'form-group', content: [
                {block: 'form-control', tag: 'textarea', content: 'Текст'}
            ]},
            {block: 'form-group', content: [
                {block: 'checkbox',
                    name: 'check_1',
                    checked: true,
                    content: 'Чекбокс'
                },
                {block: 'checkbox',
                    name: 'check_2',
                    content: 'Чекбокс'
                },
                {block: 'checkbox',
                    name: 'check_1',
                    disabled: true,
                    content: 'Чекбокс (disabled)'
                },
            ]},
            {block: 'form-group', content: [
                {block: 'radio',
                    name: 'radio_1',
                    content: 'Радио',
                    checked: true
                },
                {block: 'radio',
                    name: 'radio_1',
                    content: 'Радио'
                },
                {block: 'radio',
                    name: 'radio_1',
                    content: 'Радио (disabled)',
                    disabled: true
                },
            ]},


            ///
            {block: 'h', size: '1', content: 'Простые доп. блоки'},
            {block: 'hr'},
            {block: 'list-inline', content: [
                'Пункт 1',
                'Пункт 2',
                'Пункт 3'
            ]},
            {block: 'hr'},
            {block: 'list-unstyled', content: [
                'Пункт 1',
                ['Пункт 2 (вложеный)', [
                    {block: 'list-unstyled', content: [
                        'Пункт 1',
                        'Пункт 2',
                        'Пункт 3'
                    ]}
                ]],
                'Пункт 3'
            ]},
            {block: 'hr'},
            {block: 'list-unstyled', content: [
                {block: 'btn', mods: {color: 'default'}, content: 'default'},
                {block: 'btn', mods: {color: 'primary'}, content: 'primary'},
                {block: 'btn', mods: {color: 'warning'}, content: 'warning'},
                {block: 'btn', mods: {color: 'danger'}, content: 'danger'},
                {block: 'btn', mods: {color: 'info'}, content: 'info'},
                {block: 'btn', mods: {color: 'success'}, content: 'success'}
            ]},
            {block: 'hr'},
            {block: 'list-unstyled', content: [
                {block: 'btn', mods: {size: 'lg'}, content: 'default'},
                {block: 'btn', mods: {size: 'md'}, content: 'default'},
                {block: 'btn', mods: {size: 'sm'}, content: 'default'},
                {block: 'btn', mods: {size: 'xs'}, content: 'default'}
            ]},
            {block: 'hr'},
            {block: 'table', content: [
                {elem: 'caption', content: 'Таблица для верстальщика (default)'},
                {elem: 'thead', content: [
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]},
                {elem: 'tbody', content: [
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]},
                {elem: 'tfoot', content: [
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]}
            ]},
            {block: 'hr'},
            {block: 'table', mods: {color: 'striped'}, content: [
                {elem: 'caption', content: 'Таблица для верстальщика (striped)'},
                {elem: 'thead', content: [
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]},
                {elem: 'tbody', content: [
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]},
                {elem: 'tfoot', content: [
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]}
            ]},
            {block: 'hr'},
            {block: 'table', mods: {type: 'bordered'}, content: [
                {elem: 'caption', content: 'Таблица для верстальщика (bordered)'},
                {elem: 'thead', content: [
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]},
                {elem: 'tbody', content: [
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]},
                {elem: 'tfoot', content: [
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]}
            ]},
            {block: 'hr'},
            {block: 'table', mods: {state: 'hover'}, content: [
                {elem: 'caption', content: 'Таблица для верстальщика (hover)'},
                {elem: 'thead', content: [
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]},
                {elem: 'tbody', content: [
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]},
                {elem: 'tfoot', content: [
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]}
            ]},
            {block: 'hr'},
            {block: 'table', mods: {size: 'condensed'}, content: [
                {elem: 'caption', content: 'Таблица для верстальщика (condensed)'},
                {elem: 'thead', content: [
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]},
                {elem: 'tbody', content: [
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]},
                {elem: 'tfoot', content: [
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]}
            ]},
            {block: 'hr'},
            {block: 'table', mods: {responsive: true}, content: [
                {elem: 'caption', content: 'Таблица для верстальщика (responsive)'},
                {elem: 'thead', content: [
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]},
                {elem: 'tbody', content: [
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка'],
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]},
                {elem: 'tfoot', content: [
                    ['Ячейка', 'Ячейка', 'Ячейка', 'Ячейка', 'Ячейка']
                ]}
            ]},


        ]}
    ]
};
